'use strict';
const path = require('path');
const express = require('express');
const router = express.Router();
const mysql = require('mysql2');
const { devlog } = require(path.resolve(__dirname, '../devlog'));
const config = require(path.resolve(__dirname, '../config'));

const db = mysql.createConnection({
  host: config.dbHost,
  port: config.dbPort,
  user: config.dbUser,
  password: config.dbPassword,
  database: config.dbDatabase
});

/* GET assets listing */
router.get('/', function(req, res, next) {
  res.render('assets', { 'title': 'Assets' });
});

router.post('/', function(req, res, next) {
  if (!req.body.hostname ||
      !req.body.ip_list ||
      !req.body.users_list ||
      !req.body.mac_list) {
    devlog(`POST from ${req.ip} with strange body:`, req.body);
    return res.sendStatus(400);
  }

  const assetId = idFromHostname(req.body.hostname);
  if (!assetId) return res.sendStatus(403);

  const selectRow = 'SELECT last_update FROM heartbeats WHERE `asset_id` = "?"';
  const insertRow = 'INSERT INTO `heartbeats` (asset_id, source, ' +
    'mac_list, ip_list, users_list) VALUES ("?", "?", "?", "?", "?")';
  const deleteRow = 'DELETE FROM `heartbeats` WHERE `asset_id` = "?"';
  const data = [
    assetId,
    req.ip,
    JSON.stringify(req.body.mac_list),
    JSON.stringify(req.body.ip_list),
    JSON.stringify(req.body.users_list)
  ];

  db.query(selectRow, [assetId], (err, results) => {
    if (err) throw err;

    if (results.length > 0) {
      db.query(deleteRow, assetId, (err) => {
        if (err) throw err;
        devlog(`Removed row for Asset #${assetId}`);
      });
    }

    db.query(insertRow, data, (err) => {
      if (err) throw err;
      devlog(`Created row for Asset #${assetId}`);
    });
  });

  res.sendStatus(200);

  /* Hoisted functions */
  function idFromHostname(h) {
    const matches = h.match(/^PILOT-[0-9]{6}$/);

    if (matches && matches.length > 0) return matches[0].split('-')[1];
    else devlog(`Heartbeat from ${req.ip} with strange hostname: ${h}`);

    return false;
  }
});

module.exports = router;
