-- Setup heartbeats table.
CREATE TABLE `assetdb`.`heartbeats` (
  `asset_id` VARCHAR(45) NOT NULL,
  `source` VARCHAR(45) NULL,
  `mac_list` MEDIUMTEXT NULL,
  `ip_list` MEDIUMTEXT NULL,
  `users_list` MEDIUMTEXT NULL,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`asset_id`));
