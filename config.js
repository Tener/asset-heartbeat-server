/**
 * Default config for development.
 *
 * For production, please set environment variables instead of modifying this
 * file.
 */

const config = {
  // Node.js REST and WWW server port
  nodePort: process.env.PORT || 3000,

  // Location and credentials for MySQL database
  dbHost: process.env.MYSQL_HOST || 'localhost',
  dbPort: process.env.MYSQL_PORT || 3306,
  dbUser: process.env.MYSQL_USER || 'admin',
  dbPassword: process.env.MYSQL_PASSWORD || 'admin',
  dbDatabase: process.env.MYSQL_DATABASE || 'assetdb'
}

module.exports = config;
