/**
 * Print to console when NODE_ENV is unset or not set to 'production'
 */
exports.devlog = function devlog(...args) {
  if (process.env.NODE_ENV !== 'production') {
    console.log(...args);
  }
}
