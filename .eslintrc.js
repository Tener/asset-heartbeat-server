module.exports = {
    "extends": "standard",
    "parserOptions": {
        "ecmaVersion": 2018,
    },
    "env": {
        "es6": true,
        "node": true
    },
    "rules": {
        "new-cap": ["error", { "newIsCap": false, "capIsNew": false }],
        "semi": ["off", "never"],
        "semi-spacing": ["error", { "before": false, "after": true }],
        "space-before-function-paren": ["error", "never"],
        "max-len": ["off"],
        "no-unused-vars": ["off"]
    }
};
